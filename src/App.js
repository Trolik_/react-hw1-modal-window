import React, { useState, useEffect } from 'react';
import Button from './button';
import Modal from './modal';
import './app.scss';

const App = () => {
    const [showFirstModal, setShowFirstModal] = useState(false);
    const [showSecondModal, setShowSecondModal] = useState(false);

    const openFirstModal = () => {
        setShowFirstModal(true);
    };

    const openSecondModal = () => {
        setShowSecondModal(true);
    };

    const handleClose = () => {
        setShowFirstModal(false);
        setShowSecondModal(false);
    };

    useEffect(() => {
        const handleKeyPress = (event) => {
            if (event.key === 'Escape') {
                handleClose();
            }
        };

        document.addEventListener('keydown', handleKeyPress);

        return () => {
            document.removeEventListener('keydown', handleKeyPress);
        };
    }, []);

    return (
        <div className="app">
            <Button
                backgroundColor="blue"
                text="Open first modal"
                onClick={openFirstModal}
            />
            <Button
                backgroundColor="green"
                text="Open second modal"
                onClick={openSecondModal}
            />

            {showFirstModal && (
                <Modal
                    header="Do you want to delete this file?"
                    closeButton={true}
                    text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"
                    actions={
                        <>
                            <button className='FirstModal-FirstBtn'>Ok</button>
                            <button className='FirstModal-SecondBtn'>Cancel</button>
                        </>
                    }
                    onClose={handleClose}
                    modalStyle="first-modal"
                />
            )}

            {showSecondModal && (
                <Modal
                    header="Do you want to go to vacation?"
                    closeButton={true}
                    text="By clicking the OK button you will teleport to the coolest Hawaii). If CANCEL - you know what to do!"
                    actions={
                        <>
                            <button className='SecondModal-FirstBtn'>Vacation</button>
                            <button className='SecondModal-SecondBtn'>Work</button>
                        </>
                    }
                    onClose={handleClose}
                    modalStyle="second-modal"
                />
            )}
        </div>
    );
};

export default App;