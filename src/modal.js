import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

const Modal = ({ header, text, actions, onClose, modalStyle, closeButton }) => {
	const modalRef = useRef(null);

	const handleOutsideClick = (event) => {
		if (modalRef.current && !modalRef.current.contains(event.target)) {
			onClose();
		}
	};

	const handleKeyPress = (event) => {
		if (event.key === 'Escape') {
			onClose();
		}
	};

	useEffect(() => {
		document.addEventListener('keydown', handleKeyPress);
		return () => {
			document.removeEventListener('keydown', handleKeyPress);
		};
	});

	return (
		<>
		<div className="modal-overlay" onClick={handleOutsideClick} />
		<div className="modal">
			<div className={`modal-content ${modalStyle}`} ref={modalRef}>
				{closeButton && (
					<button className="close-button" onClick={onClose}>
						X
					</button>
				)}
				<h2>{header}</h2>
				<p>{text}</p>
				<div className="actions">{actions}</div>
			</div>
		</div>
		</>
	);
};


Modal.propTypes = {
	header: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired,
	actions: PropTypes.node.isRequired,
	onClose: PropTypes.func.isRequired,
	modalStyle: PropTypes.string.isRequired,
	closeButton: PropTypes.bool.isRequired,
};

export default Modal;
