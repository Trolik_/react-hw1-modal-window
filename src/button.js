import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ backgroundColor, text, onClick }) => {
	const buttonStyle = {
		backgroundColor: backgroundColor,
	};

	return (
		<button className="button" style={buttonStyle} onClick={onClick}>
			{text}
		</button>
	);
};

Button.propTypes = {
	backgroundColor: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
};

export default Button;
